package com.bwalczak.LicencjatApplication.entities;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

class ProductTest {

    @Test
    public void shouldGetName() {
        Product product = new Product(4, "Marchew", 1);

        assertThat(product.getName().equals("Marchew"));
    }

    @Test
    public void shouldGetAmount() {
        Product product = new Product(4, "Marchew", 1);

        assertThat(product.getAmount() == 1);
    }

    @Test
    public void shouldSetName() {
        Product product = new Product(4, "Marchew", 1);

        product.setName("Banany");

        assertThat(product.getName().equals("Banany"));
    }

    @Test
    public void shouldSetAmount() {
        Product product = new Product(4, "Marchew", 1);

        product.setAmount(4);

        assertThat(product.getAmount() == 4);
    }

}
