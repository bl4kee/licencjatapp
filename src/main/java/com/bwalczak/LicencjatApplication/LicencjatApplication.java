package com.bwalczak.LicencjatApplication;

import com.bwalczak.LicencjatApplication.dao.ProductDAO;
import com.bwalczak.LicencjatApplication.entities.Product;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class LicencjatApplication {

	public static void main(String[] args) {

		SpringApplication.run(LicencjatApplication.class, args);

	}

	@Bean
	CommandLineRunner init(ProductDAO productDAO) {
		return args -> {
			productDAO.save(new Product(1, "Pomidory", 2));
			productDAO.save(new Product(2, "Woda", 3));
			productDAO.save(new Product(3, "Czekolada", 5));
		};
	}

}
