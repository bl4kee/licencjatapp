package com.bwalczak.LicencjatApplication.controllers;

import com.bwalczak.LicencjatApplication.dao.ProductDAO;
import com.bwalczak.LicencjatApplication.entities.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@Controller
public class ProductController {

    @Autowired
    ProductDAO productDAO;

    @GetMapping("/init")
    public String showForm(Product product) {
        return "add-product";
    }

    @RequestMapping(value = {"/", "/list"}, method = RequestMethod.GET)
    public String showProductsList(Model model) {
        model.addAttribute("products", productDAO.findAll());
        return "list";
    }

    @PostMapping("/addproduct")
    public String addProduct(@Valid Product product, BindingResult result, Model model) {
        if (result.hasErrors()) {
            return "add-product";
        }

        productDAO.save(product);
        return "redirect:/list";
    }

    @GetMapping("/delete/{id}")
    public String deleteProduct(@PathVariable("id") long id, Model model) {
        Product product = productDAO.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Invalid product id: " + id));
        productDAO.delete(product);
        return "redirect:/list";
    }
}
