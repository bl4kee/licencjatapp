package com.bwalczak.LicencjatApplication.dao;

import com.bwalczak.LicencjatApplication.entities.Product;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductDAO extends CrudRepository<Product, Long> {}
