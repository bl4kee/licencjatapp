pipeline {

  agent any

  tools {
    maven 'Maven'
  }

  stages {

    stage("Budowanie") {
      steps {
         echo 'Aplikacja jest budowana...'
         sh 'mvn -B -DskipTests clean package'
         script {
             env.AUTHOR_NAME = sh(script: 'git show -s --pretty=%an', returnStdout: true)
         }
      }

      post {
        success {
           slackSend color: "good", message: 'Budowanie zakończone sukcesem: ' + "${GIT_BRANCH}" +
                                             '\nRozpoczęte przez: ' + "${env.AUTHOR_NAME}" +
                                             '\nWprowadzone zmiany: ' + "${GIT_COMMIT.substring(0,6)}" +
                                             '\nSprawdź status: ' + "${BUILD_URL}"
        }
        failure {
           slackSend color: "red", message: 'Budowanie zakończone niepowodzeniem: ' + "${GIT_BRANCH}" +
                                            '\nRozpoczęte przez: ' + "${env.AUTHOR_NAME}" +
                                            '\nWprowadzone zmiany: ' + "${GIT_COMMIT.substring(0,6)}" +
                                            '\nSprawdź status: ' + "${BUILD_URL}"
        }
      }
    }

    stage("Testowanie") {
      steps {
        echo 'Aplikacja jest testowana...'
        sh 'mvn test'
      }
    }

    stage('Statyczna analiza kodu') {

        environment {
            scannerHome = tool 'sonar'
        }

        steps {
            withSonarQubeEnv('sonarqube') {
                sh 'mvn clean package sonar:sonar'
            }
        }
    }

    stage("Wdrożenie produkcyjne") {
      steps {
        script {
           timeout(time: 2, unit: 'MINUTES') {
            input(id: "Deploy Gate", message: "Czy chcesz wdrożyć na środowisko produkcyjne?", ok: "TAK")
           }
        }
        echo 'Aplikacja jest wdrażana...'
        sh 'mvn clean heroku:deploy'
      }
    }
  }
}
